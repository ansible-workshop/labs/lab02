# Ansible Workshop
Lab 02: Hosts

---

# Instructions

- Edit the hosts file
- Run a test to verify a connection exists with managed hosts
- Create 2 groups and a group of groups
- Verify connection

## Edit the hosts file

### Open hosts file in editor
```
$ sudo vim /etc/ansible/hosts
press i
```
### Add the following entry - instead of (nodeip) enter the node ip (without brackets) you recieved - for example - 146.148.127.157
```
[demoservers]
(nodeips)
```

### Save entry and exit to terminal
```
press esc
press :wq
```

## Run the following command to run command on all servers in the group

```
$ ansible demoservers -a "echo hello Ansible"
```


### Open hosts file in editor
```
$ sudo vim /etc/ansible/hosts
press i
```
### Overwrite the existing entry with the following one - complete the IP addresses for the your nodes.
```
[demoservergroup1]
(nodeip1)

[demoservergroup2]
(nodeip2)

[demogroup:children]
demoservergroup1
demoservergroup2

```

### Save entry and exit to terminal
```
press esc
press :wq
```

## Run the following command to run command on all servers in the group

```
$ ansible demogroup -a "echo hello Ansible"
```
